## vue3.2+ts+elementPlus+vite4

整合了最基础的登录页面，修改密码页面，多语言，主题色切换，暗黑模式，elementPlus，pinia 状态管理，sass 预处理器<br />
整合百度富文本编辑器，带模板功能<br />
封装图片选择功能，图片裁剪，图片拖拽排序<br />

本框架预览：[http://vue3.88an.top](http://vue3.88an.top)

### 本框架实战《企业官网》项目演示，后端使用 Thinkphp6

企业官网模板：[http://site.88an.top](http://site.88an.top) <br />
企业官网后台：[http://site.88an.top/vue](http://site.88an.top/vue) <br />
企业官网源码：[https://gitee.com/chenguangan/site-tp6-vue3](https://gitee.com/chenguangan/site-tp6-vue3) <br />

#### 框架页面截图

登录<br />
![截图](./img/1.png)

路由切换过度动画<br />
![截图](./img/2.gif)

多语言切换<br />
![截图](./img/3.png)

暗黑模式<br />
![截图](./img/4.png)

图片选择，拖拽排序<br />
![截图](./img/5.gif)

富文本编辑，模板编辑，图片使用公共图片弹层组件<br />
![截图](./img/6.gif)

主题颜色切换<br />
![截图](./img/7.png)

修改密码<br />
![截图](./img/8.png)

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```
