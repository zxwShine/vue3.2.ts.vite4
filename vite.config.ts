import { fileURLToPath, URL } from "node:url";
import { defineConfig, loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";
import AutoImport from "unplugin-auto-import/vite";

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  // 根据当前工作目录中的 `mode` 加载 .env 文件
  // 设置第三个参数为 '' 来加载所有环境变量，而不管是否有 `VITE_` 前缀。
  const env = loadEnv(mode, process.cwd(), "");

  return {
    plugins: [
      vue(),
      AutoImport({
        imports: ["vue", "vue-router"], // 自动导入vue和vue-router相关函数
        dts: "src/auto-import.d.ts", // 生成 `auto-import.d.ts` 全局声明
      }),
    ],
    resolve: {
      alias: {
        "@": fileURLToPath(new URL("./src", import.meta.url)),
      },
    },
    server: {
      host: "0.0.0.0", //配置局域网ip访问
      port: 4000, // 设置服务启动端口号
      open: true, // 设置服务启动时是否自动打开浏览器
      cors: true, // 允许跨域

      //设置代理，根据我们项目实际情况配置
      proxy: {
        "/myApi": {
          target: env.VITE_BASE_URL,
          changeOrigin: true,
          secure: false,
          rewrite: (path) => path.replace("/myApi/", "/"),
        },
      },
    },
    base: "./", // 设置打包路径
    // 设置资源打包资源访问路径
    // base: env.VITE_MODE === "production" ? env.VITE_BASE_URL+"vue3" : "/",
    // css:{
    //   preprocessorOptions:{
    //     scss:{
    //       additionalData: `@use "@/assets/public.scss" as *;`
    //     }
    //   }
    // }
  };
});
