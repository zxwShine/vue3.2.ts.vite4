export function useImgHook() {
  //图片选择组件选项
  const imgOption = reactive({
    option: {
      name: "", //需要操作的字段名称
      index: -1, //需要替换的图片数组index下标
      index1: -1, //多维数组操作需要用的备用参数
      index2: -1, //多维数组操作需要用的备用参数
      index3: -1, //多维数组操作需要用的备用参数
    },
    show: false, //是否显示图片选择弹窗
  });

  //name 是需要操作的图片字段名称
  //如果是替换 才会有index参数返回，index就是需要替换的数组下标
  function pickImg(
    name: string,
    index?: number,
    index1?: number,
    index2?: number,
    index3?: number
  ) {
    imgOption.option.name = name;
    imgOption.option.index = index ?? -1;
    imgOption.option.index1 = index1 ?? -1;
    imgOption.option.index2 = index2 ?? -1;
    imgOption.option.index3 = index3 ?? -1;
    imgOption.show = true;
  }

  return { imgOption, pickImg };
}