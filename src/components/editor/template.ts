export default [
  `<section style="margin:10px 0px;"><section style="height:1px;background-color:#5e7d38;"></section><section style="display:flex;justify-content:center;align-items:flex-start;"><section style="width:22%;margin-top:15px;" data-width="22%"><section style="width:45px;height:45px;border-radius:50%;background-color:#ffcd25;margin:auto;"><section style="font-size:17px;color:#fefefe;text-align:center;line-height:45px;"><p><em><strong><span class="_num" data-form="0" data-num="1">01</span></strong></em></p></section></section></section><section style="width:78%;border-left:solid 1px #5e7d38;padding:20px 10px 20px 0px;" data-width="78%"><section style="display:inline-block;background-color:#5e7d38;padding:5px 25px;"><section style="font-size:16px;letter-spacing:2px;color:#fefefe;"><p><strong>保持心态</strong></p></section></section><section style="font-size:14px;letter-spacing:2px;line-height:1.75;margin:10px 0px 0px 10px;"><p>军训是学校实施素质教育，加强德育工作的一项重要内容,能够迅速增进同学之间的感情觉悟。</p></section></section></section><section style="display:flex;justify-content:center;align-items:flex-start;" class="_li"><section style="width:22%;" data-width="22%"><section style="width:45px;height:45px;border-radius:50%;background-color:#ffcd25;margin:auto;"><section style="font-size:17px;color:#fefefe;text-align:center;line-height:45px;"><p><em><strong><span class="_num" data-form="0" data-num="2">02</span></strong></em></p></section></section></section><section style="width:78%;border-left:solid 1px #5e7d38;padding:5px 10px 20px 0px;" data-width="78%"><section style="display:inline-block;background-color:#5e7d38;padding:5px 25px;"><section style="font-size:16px;letter-spacing:2px;color:#fefefe;"><p><strong>保持心态</strong></p></section></section><section style="font-size:14px;letter-spacing:2px;line-height:1.75;margin:10px 0px 0px 10px;"><p>军训是学校实施素质教育，加强德育工作的一项重要内容,能够迅速增进同学之间的感情觉悟。</p></section></section></section><section style="display:flex;justify-content:center;align-items:flex-start;" class="_li"><section style="width:22%;" data-width="22%"><section style="width:45px;height:45px;border-radius:50%;background-color:#ffcd25;margin:auto;"><section style="font-size:17px;color:#fefefe;text-align:center;line-height:45px;"><p><em><strong><span class="_num" data-form="0" data-num="3">03</span></strong></em></p></section></section></section><section style="width:78%;border-left:solid 1px #5e7d38;padding:5px 10px 20px 0px;" data-width="78%"><section style="display:inline-block;background-color:#5e7d38;padding:5px 25px;"><section style="font-size:16px;letter-spacing:2px;color:#fefefe;"><p><strong>保持心态</strong></p></section></section><section style="font-size:14px;letter-spacing:2px;line-height:1.75;margin:10px 0px 0px 10px;"><p>军训是学校实施素质教育，加强德育工作的一项重要内容,能够迅速增进同学之间的感情觉悟。</p></section></section></section></section>`,
  `<section style="margin:10px 0px;"><section style="text-align:left;margin-bottom:-25px;"><section style="display:inline-block;"><section style="display:flex;justify-content:flex-start;align-items:center;"><section style="font-size:17px;color:#fff;background-color:#11be33;padding:4px 20px;"><p><strong>节能宣传</strong></p></section><section style="display:inline-block;"><section style="border-top:17px solid #0bbc2e;border-right:12px solid transparent;line-height:0px;"></section><section style="border-bottom:18px solid #0bbc2e;border-right:12px solid transparent;line-height:0px;"></section></section></section><section style="display:flex;justify-content:flex-start;align-items:center;"><section style="border-top:15px solid #1f8e35;border-left:15px solid transparent;line-height:0px;"></section></section></section></section><section class="_editor"><section style="border-left:1px solid #11be33;padding:25px 0 0px;padding-left:15px;margin-left:15px;"><section style="display:flex;justify-content:flex-start;align-items:center;"><section style="border-top:5px solid transparent;border-right:10px solid #11be33;border-bottom:5px solid transparent;line-height:0px;margin-left:-15px;"></section><section style="width:100%;font-size:14px;color:#fff;background-color:#11be33;border-radius:10px;letter-spacing:1.5px;padding:5px 10px;"><p>积极应对气候变化，推动绿色低碳发展</p></section></section><section style="color:#3e3a39;font-size:14px;letter-spacing:2px;text-align:left;line-height:1.75;margin-top:10px;"><p style="text-align:justify;">2023年7月12日是全国低碳日。全国低碳日宣传活动，围绕“积极应对气候变化，推动绿色低碳发展”主题。</p></section></section></section><section class="_editor"><section style="border-left:1px solid #11be33;padding:25px 0 0px;padding-left:15px;margin-left:15px;"><section style="display:flex;justify-content:flex-start;align-items:center;"><section style="border-top:5px solid transparent;border-right:10px solid #11be33;border-bottom:5px solid transparent;line-height:0px;margin-left:-15px;"></section><section style="width:100%;font-size:14px;color:#fff;background-color:#11be33;border-radius:10px;letter-spacing:1.5px;padding:5px 10px;"><p>积极应对气候变化，推动绿色低碳发展</p></section></section><section style="color:#3e3a39;font-size:14px;letter-spacing:2px;text-align:left;line-height:1.75;margin-top:10px;"><p style="text-align:justify;">2023年7月12日是全国低碳日。全国低碳日宣传活动，围绕“积极应对气候变化，推动绿色低碳发展”主题。</p></section></section></section></section>`,
  `<section style="margin:10px 0px;"><section class="_editor"><section style="display:flex;justify-content:flex-start;align-items:center;border-left:1px solid #01a581;margin-left:10px;padding:10px 0;"><section style="width:10px;height:10px;background-color:#01a581;border-radius:100%;margin-left:-7px;"></section><section style="width:10px;height:10px;border-bottom:1px solid #01a581;border-left:1px solid #01a581;transform:rotateZ(45deg);margin:0 15px;"></section><section style="width:90%;" data-width="90%"><section style="display:flex;justify-content:flex-start;align-items:center;"><section style="display:inline-block;margin-left:20px;margin-bottom:-0.7em;background-color:#FEFEFF;"><section style="display:flex;justify-content:flex-start;align-items:center;"><section style="font-size:13px;color:#01a581;letter-spacing:1px;background-color:#FEFEFF;padding:0 10px;"><p>倡议书</p></section><section style="width:6px;height:6px;background-color:#01a581;border-radius:100%;margin-left:5px;"></section></section></section></section><section style="padding-bottom:15px;"><section style="border:1px solid #01a581;padding:0 8px;"><section style="color:#fff;font-size:14px;letter-spacing:2px;text-align:left;line-height:1.75;background-color:#01a581;padding:10px;margin:15px 0 -15px 0;"><p>推广公共交通和步行：尽量减少私人汽车的使用，选择公共交通工具或步行、骑自行车出行。</p></section></section></section></section></section></section><section class="_editor"><section style="display:flex;justify-content:flex-start;align-items:center;border-left:1px solid #01a581;margin-left:10px;padding:10px 0;"><section style="width:10px;height:10px;background-color:#01a581;border-radius:100%;margin-left:-7px;"></section><section style="width:10px;height:10px;border-bottom:1px solid #01a581;border-left:1px solid #01a581;transform:rotateZ(45deg);margin:0 15px;"></section><section style="width:90%;" data-width="90%"><section style="display:flex;justify-content:flex-start;align-items:center;"><section style="display:inline-block;margin-left:20px;margin-bottom:-0.7em;background-color:#FEFEFF;"><section style="display:flex;justify-content:flex-start;align-items:center;"><section style="font-size:13px;color:#01a581;letter-spacing:1px;background-color:#FEFEFF;padding:0 10px;"><p>倡议书</p></section><section style="width:6px;height:6px;background-color:#01a581;border-radius:100%;margin-left:5px;"></section></section></section></section><section style="padding-bottom:15px;"><section style="border:1px solid #01a581;padding:0 8px;"><section style="color:#fff;font-size:14px;letter-spacing:2px;text-align:left;line-height:1.75;background-color:#01a581;padding:10px;margin:15px 0 -15px 0;"><p>推广公共交通和步行：尽量减少私人汽车的使用，选择公共交通工具或步行、骑自行车出行。</p></section></section></section></section></section></section></section>`,
  `<section data-align="title" style="text-align:center;margin:10px 0px;" class=""><section style="display:inline-block;width:80%;padding-top:10px;" data-width="80%" class=""><section style="display:flex;justify-content:space-between;align-items:center;border-top:1px solid #e64d46;"><section style="display:inline-block;margin-left:10px;margin-top:-10px;"><section style="width:35px;height:35px;line-height:45px;color:#fff;background-color:#e64d46;font-size:16px;"><p><strong>01</strong></p></section><section style="display:flex;justify-content:center;align-items:center;"><section style="border-top:20px solid #e64d46;border-right:35px solid transparent;line-height:0px;"></section></section></section><section style="font-size:17px;line-height:17px;color:#e64d46;letter-spacing:2px;padding-right:5px;"><p><strong>这里写标题</strong></p></section></section></section></section>`,
  `<section class="wwei-editor"><h2 style="padding: 0px; margin: 8px 0px 0px; font-weight: normal; font-size: 16px; font-family: 微软雅黑; white-space: normal; height: 32px; text-align: justify; color: rgb(62, 62, 62); line-height: 18px; border-bottom-color: rgb(227, 227, 227); border-bottom-width: 1px; border-bottom-style: solid;"><span style="padding: 0px 2px 3px; color: rgb(0, 112, 192); line-height: 28px; border-bottom-color: rgb(0, 187, 236); border-bottom-width: 2px; border-bottom-style: solid; float: left; display: block;"><span style="padding: 4px 10px; border-top-left-radius: 80%; border-top-right-radius: 100%; border-bottom-right-radius: 90%; border-bottom-left-radius: 20%; color: rgb(255, 255, 255); margin-right: 8px; background-color: rgb(0, 187, 236);">序号.</span><span style="color: rgb(0, 187, 236);">标题党</span></span></h2></section>`,
  `<section class="wwei-editor"><blockquote style="padding: 10px; margin: 5px 0px 0px; white-space: normal; max-width: 100%; line-height: 25px; font-size: 14px; font-family: arial, helvetica, sans-serif; border-top-left-radius: 4px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; color: rgb(255, 255, 255); border-left-color: rgb(0, 187, 236); border-left-width: 10px; border-left-style: solid; box-shadow: rgb(153, 153, 153) 2px 2px 4px; text-shadow: rgb(34, 95, 135) 0px 1px 0px; word-wrap: break-word !important; box-sizing: border-box !important; background-color: rgb(55, 57, 57);"><span style="max-width: 100%; word-wrap: break-word !important; box-sizing: border-box !important;"><span style="max-width: 100%; font-family: 微软雅黑; font-size: 16px; word-wrap: break-word !important; box-sizing: border-box !important;">1、这里输入标题</span></span></blockquote></section>`,
  `<section class="wwei-editor"><section style="max-width: 100%;margin: 0.8em 0px 0.5em; overflow: hidden; "><section placeholder="请输入标题"style="box-sizing: border-box !important;  height:36px;display: inline-block;color: #FFF; font-size: 16px;font-weight:bold; padding:0 10px;line-height: 36px;float: left; vertical-align: top; background-color: rgb(249, 110, 87); "class="wweibrush">请输入标题</section><section style="box-sizing: border-box !important; display: inline-block;height:36px; vertical-align: top; border-left-width: 9px; border-left-style: solid; border-left-color: rgb(249, 110, 87); border-top-width: 18px !important; border-top-style: solid !important; border-top-color: transparent !important; border-bottom-width: 18px !important; border-bottom-style: solid !important; border-bottom-color: transparent !important;"></section></section></section>`,
  `<section class="wwei-editor"><h2 style="margin: 8px 0px 0px; padding: 0px; font-weight:bold;font-size:16px;line-height:28px;  max-width: 100%;color:rgb(0, 112, 192); min-height: 32px;border-bottom: 2px solid rgb(0, 112, 192); text-align: justify;"><span class="autonum"placeholder="1"style="background-color:rgb(0, 112, 192); border-radius:80% 100% 90% 20%; color:rgb(255, 255, 255); display:block; float:left; line-height:20px; margin:0px 8px 0px 0px; max-width:100%; padding:4px 10px; word-wrap:break-word !important">1</span><strong class="wweibrush"data-brushtype="text">第一标题</strong></h2></section>`,
  `<section class="wwei-editor"><h2 class="wweibrush"data-bcless="darken"placeholder="深色边框标题"style="margin: 10px 0px; padding: 10px; font-size: 16px; line-height: 25px; text-shadow: rgb(34, 95, 135) 0px 1px 0px; color: rgb(202, 251, 215); border-radius: 4px; box-shadow: rgb(153, 153, 153) 2px 2px 4px; border-left-width: 10px; border-left-style: solid; border-color: rgb(10, 137, 43); background-color: rgb(14, 184, 58);">深色边框标题</h2></section>`,
  `<section class="wwei-editor"><section style="margin: 2px 0.8em 1em 0; text-align: center; font-size: 1em; vertical-align: middle; white-space: nowrap;"><section style="height: 0px; white-space: nowrap;  border-top: 1.5em solid rgb(71, 193, 168); border-bottom: 1.5em solid rgb(71, 193, 168); border-left-width: 1.5em ! important; border-left-style: solid ! important; border-right-width: 1.5em ! important; border-right-style: solid ! important; border-color: rgb(71, 193, 168);"></section><section style="height: 0; white-space: nowrap; margin: -2.75em 1.65em;border-top: 1.3em solid #ffffff;border-bottom: 1.3em solid #ffffff;border-left: 1.3em solid transparent;border-right: 1.3em solid transparent;"></section><section style="height: 0px; white-space: nowrap; margin: 0.45em 2.1em; vertical-align: middle; border-top: 1.1em solid rgb(71, 193, 168); border-bottom: 1.1em solid rgb(71, 193, 168); border-left-width: 1.1em ! important; border-left-style: solid ! important; border-right-width: 1.1em ! important; border-right-style: solid ! important; border-color: rgb(71, 193, 168);"><section class="wweibrush"data-ct="fix"placeholder="一行短标题"style="max-height: 1em; padding: 0px; margin-top: -0.5em; color: rgb(254, 255, 253); font-size: 1.2em; line-height: 1em; white-space: nowrap; overflow: hidden; font-style: normal;">一行短标题</section></section></section></section>`,
  `<section class="wwei-editor"><h2 class="wweibrush"placeholder="请输入标题"style="white-space: normal; font-size: 16px; margin: 10px 0px; padding: 10px; max-width: 100%; border-top:solid 2px;border-left:0px; border-bottom:solid 2px; line-height: 25px; color: rgb(109, 151, 200);font-weight:bold; text-align: center;">请输入标题</h2></section>`,
  `<section class="wwei-editor"><fieldset style="clear: both; padding: 0px; border: 0px none; margin: 1em 0px 0.5em;"><section style="border-top-width: 2px; border-top-style: solid; border-color: rgb(142, 201, 101); font-size: 1em; font-weight: inherit; text-decoration: inherit; color: rgb(255, 255, 255); box-sizing: border-box;"><section class="wweibrush"data-brushtype="text"style="padding: 0px 0.5em; background-color: rgb(142, 201, 101); display: inline-block; font-size: 16px;">微信编辑器</section></section></fieldset></section>`,
  `<section class="wwei-editor"><section style="font-size: 1em; white-space: normal; margin: 1em 0px 0.8em; text-align: center; vertical-align: middle;"><section style="height: 0px; margin: 0px 1em; border: 1.5em solid rgb(255, 202, 0); border-left-color: transparent !important;border-right-color: transparent !important;"></section><section style="height: 0px; margin: -2.75em 1.65em; border-width: 1.3em; border-style: solid; border-color: rgb(255, 255, 255) transparent;"></section><section style="height: 0px; margin: 0.45em 2.1em; vertical-align: middle; border:1.1em solid rgb(255, 202, 0); border-left-color: transparent !important;  border-right-color: transparent !important;"><section class="wweibrush"data-brushtype="text"placeholder="一行短标题"style="max-height: 1em; padding: 0px; margin-top: -0.5em; color: rgb(255, 255, 255); font-size: 1.2em; line-height: 1em; overflow: hidden;">一行短标题</section></section></section></section>`,
  `<section class="wwei-editor"><section style="border: 3px solid rgb(255, 129, 36); padding: 5px;"><section data-bcless="lighten"style="border: 1px solid rgb(255, 158, 87); padding: 15px; text-align: center; color: inherit;"><p style="color: inherit;">微信编辑器</p><p style="color:inherit; font-size:24px"><strong style="color:inherit"><span class="wweibrush"data-brushtype="text"style="color:inherit; font-size:24px">操作方便才是硬道理</span></strong></p></section></section></section>`,
  `<section class="wwei-editor"><p class="wweibrush"placeholder="请输入标题"style="max-width: 100%; word-wrap: normal; min-height: 1em; white-space: pre-wrap;line-height: 25px;font-size:20px;box-sizing:border-box !important;text-shadow:rgb(0, 187, 236) 1px 0px 4px, rgb(0, 187, 236) 0px 1px 4px, rgb(0, 187, 236) 0px -1px 4px, rgb(0, 187, 236) -1px 0px 4px; word-wrap:break-word !important;color:rgb(255, 255, 255);font-weight:bold;">请输入标题</p></section>`,
  `<section class="wwei-editor"><fieldset style="border: 0px; text-align: center; box-sizing: border-box; padding: 0px;"><section style="display: inline-block; box-sizing: border-box; color: inherit;"><section class="wweibrush"data-brushtype="text"style="margin: 0.2em 0px 0px; padding: 0px 0.5em 5px; max-width: 100%; color: rgb(107, 77, 64); font-size: 1.8em; line-height: 1; border-bottom-width: 1px; border-bottom-style: solid; border-color: rgb(107, 77, 64);">微信编辑器</section><section class="wweibrush"data-brushtype="text"style="margin: 5px 1em; font-size: 1em; line-height: 1; color: rgb(107, 77, 64); box-sizing: border-box; border-color: rgb(107, 77, 64);">做最易用的编辑器</section></section></fieldset></section>`,
  `<section class="wwei-editor"><p><span style="border-color:rgb(30, 155, 232); color:rgb(30, 155, 232); font-size:4em; font-weight:bolder; line-height:1em; vertical-align:middle">“</span><span class="wweibrush"data-brushtype="text"style="color:inherit; font-size:2em; font-style:normal; line-height:1.2em; vertical-align:middle">标题</span><span class="wweibrush"data-brushtype="text"style="border-color:rgb(30, 155, 232); color:rgb(30, 155, 232); font-size:2em; font-style:normal; line-height:1.2em; vertical-align:middle">标题</span><span style="border-color:rgb(30, 155, 232); color:rgb(30, 155, 232); font-size:4em; font-weight:bolder; line-height:1em; vertical-align:middle">”</span></p></section>`,
  `<section class="wwei-editor"><section data-bcless="true"style="font-size:20px; background: rgb(223, 240, 203);border:0 none;"><span class="wweibrush"style="background:rgb(255, 255, 255); color:rgb(150, 206, 84); display:inline-block; padding:0px 15px 0px 0px">微信编辑器</span><span style="background:none repeat scroll 0 0 #fff; display:inline-block; margin-left:5px">&nbsp;</span><span style="background:none repeat scroll 0 0 #fff; display:inline-block; margin-left:5px">&nbsp;</span><span style="background:none repeat scroll 0 0 #fff; display:inline-block; margin-left:5px">&nbsp;</span></section></section>`,
  `<section class="wwei-editor"><fieldset style="border: 0px rgb(107, 77, 64); text-align: center; margin: 0.8em 0px 0.5em; box-sizing: border-box; padding: 0px;"><section style="color: rgb(107, 77, 64); display: inline-block; width: 1em; font-size: 2.5em; font-weight: inherit; line-height: 1em; vertical-align: top; text-align: inherit; text-decoration: inherit; box-sizing: border-box; border-color: rgb(107, 77, 64);"><section class="wweibrush"data-brushtype="text"style="box-sizing: border-box; color: inherit; border-color: rgb(107, 77, 64);">两字</section></section><section style="display: inline-block; margin-left: 0.5em; margin-top: 0.2em; text-align: left; box-sizing: border-box; color: inherit;"><section style="box-sizing: border-box; color: inherit;"><section class="wweibrush"data-brushtype="text"style="background-color:rgb(107, 77, 64); border-color:rgb(107, 77, 64); box-sizing:border-box; color:rgb(224, 209, 202); display:inline-block; font-size:2em; font-weight:inherit; line-height:1; padding:0.1em; text-align:inherit; text-decoration:inherit">副标题1</section></section><section style="color: rgb(107, 77, 64); margin: 0.5em 0px; font-size: 1.5em; line-height: 1em; font-weight: inherit; text-align: inherit; text-decoration: inherit; box-sizing: border-box; border-color: rgb(107, 77, 64);"><section class="wweibrush"data-brushtype="text"style="box-sizing: border-box; color: inherit; border-color: rgb(107, 77, 64);">副标题2</section></section></section></fieldset></section>`,
  `<section class="wwei-editor"><h2 class="wweibrush"data-brushtype="text"style="padding-bottom: 9px;border-bottom: 1px solid #eee;argin-top: 20px;margin-bottom: 10px;font-weight: 500;line-height: 1.1;font-size: 22px;">极简标题</h2></section>`,
  `<section class="wwei-editor" style="color: rgb(51, 51, 51); font-family: 微软雅黑; font-size: 12px; white-space: normal; display: inline-block; height: 2em; max-width: 100%; line-height: 1em; box-sizing: border-box; border-top-width: 1.1em; border-top-style: solid; border-top-color: rgb(0, 187, 236); border-bottom-width: 1.1em; border-bottom-style: solid; border-bottom-color: rgb(0, 187, 236); border-right-width: 1em; border-right-style: solid; border-right-color: transparent;"><section style="height: 2em; margin-top: -1em; color: white; padding: 0.5em 1em; max-width: 100%; white-space: nowrap; text-overflow: ellipsis;">这里输入标题</section></section>`,
  `<section  style="margin-top: 8px;margin-right: 0%;margin-left: 0%;box-sizing: border-box;"><section
	  style="height: 1px; background-color: #0aa857; box-sizing: border-box"
	></section>
	</section>
	<section
	style="
	  margin-top: -1px;
	  margin-right: 0%;
	  margin-left: 0%;
	  box-sizing: border-box;
	"
	>
	<section
	  style="
		display: inline-block;
		min-width: 10%;
		max-width: 100%;
		vertical-align: top;
		padding-right: 10px;
		padding-left: 10px;
		border-width: 1px;
		border-radius: 0px;
		border-style: none solid solid;
		border-color: #0aa857;
		background-color: #ffffff;
		box-sizing: border-box;
	  "
	>
	  <section style="box-sizing: border-box">
		<section style="color: #ccd553; box-sizing: border-box">
		  <p style="letter-spacing: 2px">
			<span style="font-size: 18px"
			  ><strong
				><span style="color: rgb(47, 195, 127)"
				  >请输入标题</span
				></strong
			  ></span
			>
		  </p>
		</section>
	  </section>
	</section>
	</section>
	`,
  `
	<section
	style="
	  margin-top: 8px;
	  margin-right: 0%;
	  margin-left: 0%;
	  box-sizing: border-box;
	"
	>
	<section
	  style="height: 1px; background-color: #0aa857; box-sizing: border-box"
	></section>
	</section>
	<section
	style="
	  margin-top: -1px;
	  margin-right: 0%;
	  margin-left: 0%;
	  box-sizing: border-box;
	  text-align: center;
	"
	>
	<section
	  style="
		display: inline-block;
		min-width: 10%;
		max-width: 100%;
		vertical-align: top;
		padding-right: 10px;
		padding-left: 10px;
		border-width: 1px;
		border-radius: 0px;
		border-style: none solid solid;
		border-color: #0aa857;
		background-color: #ffffff;
		box-sizing: border-box;
	  "
	>
	  <section style="box-sizing: border-box">
		<section style="color: #ccd553; box-sizing: border-box">
		  <p style="letter-spacing: 2px">
			<span style="font-size: 18px"
			  ><strong
				><span style="color: rgb(47, 195, 127)"
				  >请输入标题</span
				></strong
			  ></span
			>
		  </p>
		</section>
	  </section>
	</section>
	</section>
	`,
  `
	<section
	style="
	  margin-top: 8px;
	  margin-right: 0%;
	  margin-left: 0%;
	  box-sizing: border-box;
	"
	>
	<section
	  style="height: 1px; background-color: #0aa857; box-sizing: border-box"
	></section>
	</section>
	<section
	style="
	  margin-top: -1px;
	  margin-right: 0%;
	  margin-left: 0%;
	  box-sizing: border-box;
	  text-align: right;
	"
	>
	<section
	  style="
		display: inline-block;
		min-width: 10%;
		max-width: 100%;
		vertical-align: top;
		padding-right: 10px;
		padding-left: 10px;
		border-width: 1px;
		border-radius: 0px;
		border-style: none solid solid;
		border-color: #0aa857;
		background-color: #ffffff;
		box-sizing: border-box;
	  "
	>
	  <section style="box-sizing: border-box">
		<section style="color: #ccd553; box-sizing: border-box">
		  <p style="letter-spacing: 2px">
			<span style="font-size: 18px"
			  ><strong
				><span style="color: rgb(47, 195, 127)"
				  >请输入标题</span
				></strong
			  ></span
			>
		  </p>
		</section>
	  </section>
	</section>
	</section>
	`,
  `
	<section style="margin-top: 10px">
	<section style="display: flex; align-items: center">
	  <section
		style="
		  flex: 2 1 0%;
		  border-top: 2px solid rgb(200, 50, 50);
		  box-sizing: border-box;
		"
	  >
		<section
		  style="height: 12px; background: rgb(200, 50, 50); margin-top: 5px"
		>
		  <section
			style="
			  width: 0px;
			  height: 0px;
			  border-width: 6px 0px 6px 6px;
			  border-style: solid;
			  border-color: transparent rgb(255, 255, 255);
			  box-sizing: border-box;
			"
		  ></section>
		</section>
	  </section>
	  <section
		style="
		  width: 12px;
		  border-top: 2px solid rgb(200, 50, 50);
		  margin-top: 8px;
		  transform: skew(0deg, 38deg);
		  -webkit-transform: skew(0deg, 38deg);
		  -moz-transform: skew(0deg, 38deg);
		  -ms-transform: skew(0deg, 38deg);
		  -o-transform: skew(0deg, 38deg);
		"
	  >
		<section
		  style="
			width: 12px;
			height: 12px;
			background: rgb(200, 50, 50);
			margin-top: 5px;
		  "
		></section>
	  </section>
	  <section
		style="
		  border-bottom: 12px solid rgb(200, 50, 50);
		  align-self: flex-end;
		  padding-right: 2px;
		  padding-left: 2px;
		  box-sizing: border-box;
		"
	  >
		<p
		  style="
			font-size: 16px;
			font-weight: bold;
			color: rgb(200, 50, 50);
			min-width: 1px;
		  "
		>
		  <span style="font-size: 18px">建党节</span>
		</p>
	  </section>
	  <section
		style="
		  border-bottom: 12px solid rgb(200, 50, 50);
		  align-self: flex-end;
		  padding-right: 5px;
		  padding-left: 5px;
		  box-sizing: border-box;
		"
	  >
		<p style="font-size: 12px; color: rgb(200, 50, 50); min-width: 1px">
		  历史发展
		</p>
	  </section>
	  <section
		style="
		  flex: 1 1 0%;
		  border-top: 2px solid rgb(200, 50, 50);
		  align-self: flex-end;
		  box-sizing: border-box;
		"
	  >
		<section
		  style="
			height: 12px;
			background: rgb(200, 50, 50);
			margin-top: 5px;
			text-align: right;
		  "
		>
		  <section
			style="
			  display: inline-block;
			  vertical-align: top;
			  width: 0px;
			  height: 0px;
			  border-width: 6px 6px 6px 0px;
			  border-style: solid;
			  border-color: transparent rgb(255, 255, 255);
			  box-sizing: border-box;
			"
		  ></section>
		</section>
	  </section>
	</section>
	</section>
	`,

  `
	  <section style="text-align: center">
	  <section
		style="
		  display: inline-block;
		  letter-spacing: 1.5px;
		  color: rgb(153, 189, 255);
		"
	  >
		<section
		  style="display: flex; justify-content: center; align-items: flex-end"
		>
		  <section style="font-size: 22px; margin-top: 2px">在这里</section>
		  <section
			style="
			  width: 1px;
			  height: 2.4em;
			  background: #99bdff;
			  margin: -10px 7px -10px 5px;
			  transform: rotate(30deg);
			  -webkit-transform: rotate(30deg);
			  -moz-transform: rotate(30deg);
			  -ms-transform: rotate(30deg);
			  -o-transform: rotate(30deg);
			"
		  ></section>
		  <section style="font-size: 16px">输入标题</section>
		</section>
	  </section>
	</section>
	  `,

  `
			<section style="margin: 10px 0px;">
				<section style="border-bottom: 5px solid rgb(182, 228, 255);box-sizing: border-box;">
					<section style="background-color:#79ccfd;">
						<section style="display:flex;align-items: flex-start;">
							<section
								style="background-color: rgb(254, 254, 254);display: inline-block;padding-right: 20px;padding-left: 20px;box-sizing: border-box;">
								<p style="letter-spacing: 2px;"><span style="color: rgb(29, 150, 221);"><strong>党政类新闻类标题
											<mpchecktext contenteditable="false" id="1580782639728_0.740415226261429">
											</mpchecktext>
										</strong></span></p>
							</section>
							<section
								style="border-style: solid; border-width: 13px; border-color: rgb(254, 254, 254) transparent transparent rgb(254, 254, 254); width: 0px; height: 0px; box-sizing: border-box;">
								<br>
							</section>
						</section>
					</section>
				</section>
				<section style="margin:10px;">
					<p style="letter-spacing: 2px;"><span
							style="font-size: 14px;">据中国地震台网正式测定，02月03日00时05分在四川成都市青白江区（北纬30.74度，东经104.46度）发生5.1级地震，震源深度21千米。
							<mpchecktext contenteditable="false" id="1580782639729_0.44097248624747665"></mpchecktext>
						</span></p>
				</section>
			</section>
		`,
  `
			<section style="text-align: left; margin: 10px 0px;">
				<section style="display:inline-block;">
					<section style="display: flex;justify-content:center;align-items:flex-end;">
						<section
							style="background-color:#b5b7e6;border-radius:10px 10px 0px 0px;padding:5px 15px 10px 20px;">
							<p style="letter-spacing: 2px;"><span style="color: #fefeff; letter-spacing: 2px;">简约标题正文</span>
							</p>
						</section>
						<section
							style="margin-left:0px;border-style: solid;border-width: 16px 5px;border-color: transparent transparent #b5b7e6 #b5b7e6;width: 0px;height: 0px;">
						</section>
					</section>
				</section>
				<section style="margin-top:-4px;transform: rotatez(0deg);">
					<section style="background-color:#b5b7e6;padding:1px 0px;border-radius:10px;">
						<section
							style="margin:-5px 0px 5px 0px;border:solid 4px #e4e7fb;border-radius:10px;background-color:#fefefe;padding:15px;color:#abade2;">
							<p style="letter-spacing: 2px;">&nbsp;
								&nbsp;沉静在小窗下，听着淅沥的窗外的雨落，在这秋意渐浓的夜里。丝雨，夹杂着遥远的思念，轻轻的向着那片残缺的荷蔓延开来了呢。曾几何时啊，还是那一擎擎的绿萝伞盖，而今啊，黄了绿草，焦了碧叶，那一片片的粉红也成了枯瘦的朵儿，只是还在不屈的挺立着。&nbsp;
								&nbsp; &nbsp; &nbsp;&nbsp;</p>
						</section>
					</section>
				</section>
			</section>
		`,
  `
			<section style="margin-top: 10px; margin-bottom: 10px; text-align: left;">
				<section style="display:inline-block;">
					<section style="display:flex;justify-content: center;align-items: center;">
						<section style="height: 2em; padding-top: 0.3em; ">
							<section
								style=" background-color: #ffffff; font-size:30px;  text-align: center; line-height: 1.2em; margin-top: -0.3em; color: #595959;padding:0px 5px;">
								<p class="_num" data-form="0" style="font-weight: bold; letter-spacing: 2px;" data-num="1">1
								</p>
							</section>
						</section>
						<section
							style="border-top: 1px solid #595959; line-height: 1.6em; font-size: 15px; color: #595959; border-bottom-color: #595959; border-left-color: #595959; border-right-color: #595959;">
							<p style="letter-spacing: 2px;">扶贫攻坚国内新闻</p>
						</section>
					</section>
				</section>
				<section style="border-width: 1px; border-style: solid; border-color: #595959;">
					<section style="margin-top: 10px; margin-right: 10px; margin-left: 10px;">
						<p style="text-align: left; padding: 5px; letter-spacing: 2px;"><span
								style="font-size: 15px; letter-spacing: 2px;">扶贫是保护贫困户的合法权益，取消贫困负担。政府帮助贫困地区加大人才开发、完善农民工市场。临时工基本待遇，建立发展工农业企业、促进生产摆脱贫困的一种社会工作。</span>
						</p>
					</section>
					<section
						style="margin-right:-3px;margin-bottom:-1px;background-image: linear-gradient(135deg,  #595959 50%,#fefefe 50%);margin-left:auto;width:20px;height:20px;">
					</section>
				</section>
			</section>
		`,
  `
			<section class="wwei-editor">
				<section
					style="line-height: 25.6px; white-space: normal; max-width: 100%; box-sizing: border-box; font-family: 微软雅黑, 宋体; text-align: center; word-wrap: break-word !important; background-color: rgb(255, 255, 255);">
					<section
						style="max-width: 100%; line-height: 25.6px; font-family: 微软雅黑; box-sizing: border-box !important; word-wrap: break-word !important;">
						<section
							style="max-width: 100%; box-sizing: border-box; border: 0px none; word-wrap: break-word !important;">
							<section
								style="margin: 5px auto; padding: 5px; max-width: 100%; box-sizing: border-box; border: 3px solid rgb(54, 95, 147); overflow-wrap: break-word !important;">
								<section
									style="padding: 15px; max-width: 100%; box-sizing: border-box; border: 1px dashed rgb(54, 95, 147); color: inherit; overflow-wrap: break-word !important;">
									<p
										style="padding-top: 5px; padding-bottom: 5px; max-width: 100%; min-height: 1em; line-height: 1.5em; color: rgb(61, 61, 61); letter-spacing: 1px; text-indent: 0em; text-align: left; box-sizing: border-box !important; word-wrap: break-word !important;font-size:12px;">
										在这里替换你的文字内容，注意不要用删除键把所有文字删除，请保留一个或者用鼠标选取后TXT文档复制粘贴替换，防止格式错乱。<br
											style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;">
									</p>
									<p
										style="padding-top: 5px; padding-bottom: 5px; max-width: 100%; min-height: 1em; line-height: 1.5em; color: rgb(61, 61, 61); letter-spacing: 1px; text-indent: 0em; text-align: left; box-sizing: border-box !important; word-wrap: break-word !important;font-size:12px;">
										在这里替换你的文字内容，注意不要用删除键把所有文字删除，请保留一个或者用鼠标选取后TXT文档复制粘贴替换，防止格式错乱。</p>
								</section>
							</section>
						</section>
					</section>
				</section>
			</section>
		`,
  `
			<i class="fa fa-heart-o cursor-fav"></i>
			<i class="cursor-vip v1"></i>
			<section style="margin-top: 10px;margin-right: 0%;margin-left: 0%;box-sizing: border-box;">
				<section
					style="display: inline-block;width: 100%;vertical-align: top;border-width: 0px;box-shadow: #000000 0px 0px 0px;box-sizing: border-box;">
					<section style="box-sizing: border-box;">
						<section
							style="display: inline-block;width: 100%;vertical-align: top;padding-top: 10px;padding-bottom: 10px;box-shadow: rgb(0, 0, 0) 0px 0px 0px;box-sizing: border-box;background-color: rgba(239, 64, 16, 0.729412);">
							<section style="box-sizing: border-box;">
								<section style="text-align: center;line-height: 1;color: #ffffff;box-sizing: border-box;">
									<p style="box-sizing: border-box;"> 亚运会金牌榜</p>
								</section>
							</section>
						</section>
					</section>
					<section style="text-align: center;box-sizing: border-box;">
						<section
							style="display: inline-table;border-collapse: collapse;table-layout: fixed;width: 100%;box-sizing: border-box;">
							<section style="display: table-row-group;box-sizing: border-box;">
								<section style="display: table-row;width: 100%;box-sizing: border-box;">
									<section
										style="display: table-cell;vertical-align: middle;width: 25%;border-width: 1px;border-radius: 0px;border-style: none none dashed;border-color: #3e3e3e #f8f8f8 #cac6c6 #3e3e3e;padding: 5px;box-sizing: border-box;background-color: rgba(239, 16, 16, 0.0235294);">
										<section style="box-sizing: border-box;">
											<section
												style="display: inline-block;width: 100%;border-left: 0px solid rgb(202, 198, 198);border-right: 1px solid rgb(202, 198, 198);border-top-style: solid;border-bottom-style: solid;border-top-width: 0px;border-bottom-width: 0px;box-sizing: border-box;">
												<section style="box-sizing: border-box;">
													<section style="font-size: 10px;box-sizing: border-box;">
														<p style="box-sizing: border-box;"> 冠军</p>
													</section>
												</section>
											</section>
										</section>
									</section>
									<section
										style="display: table-cell;vertical-align: middle;width: 50%;border-width: 1px;border-radius: 0px;border-style: none none dashed;border-color: #3e3e3e #f8f8f8 #cac6c6 #3e3e3e;padding: 5px;box-sizing: border-box;background-color: rgba(239, 16, 16, 0.0235294);">
										<section
											style="display: inline-block;width: 100%;border-left: 0px solid rgb(202, 198, 198);border-right: 1px solid rgb(202, 198, 198);border-top-style: solid;border-bottom-style: solid;border-top-width: 0px;border-bottom-width: 0px;box-sizing: border-box;">
											<section style="font-size: 10px;box-sizing: border-box;">
												<p style="box-sizing: border-box;"> 中国</p>
											</section>
										</section>
									</section>
									<section
										style="display: table-cell;vertical-align: middle;width: 25%;border-width: 1px;border-radius: 0px;border-style: none none dashed;border-color: #3e3e3e #f8f8f8 #cac6c6 #3e3e3e;padding: 5px;box-sizing: border-box;background-color: rgba(239, 16, 16, 0.0235294);">
										<section style="font-size: 10px;box-sizing: border-box;">
											<p style="box-sizing: border-box;"> 132</p>
										</section>
									</section>
								</section>
							</section>
							<section style="display: table-row-group;box-sizing: border-box;">
								<section style="display: table-row;width: 100%;box-sizing: border-box;">
									<section
										style="display: table-cell;vertical-align: middle;width: 25%;border-width: 1px;border-radius: 0px;border-style: none none dashed;border-color: #3e3e3e #f8f8f8 #cac6c6 #3e3e3e;padding: 5px;box-sizing: border-box;background-color: rgba(239, 16, 16, 0.0980392);">
										<section
											style="display: inline-block;width: 100%;border-left: 0px solid rgb(202, 198, 198);border-right: 1px solid rgb(202, 198, 198);border-top-style: solid;border-bottom-style: solid;border-top-width: 0px;border-bottom-width: 0px;box-sizing: border-box;">
											<section style="font-size: 10px;box-sizing: border-box;">
												<p style="box-sizing: border-box;"> 亚军</p>
											</section>
										</section>
									</section>
									<section
										style="display: table-cell;vertical-align: middle;width: 50%;border-width: 1px;border-radius: 0px;border-style: none none dashed;border-color: #3e3e3e #f8f8f8 #cac6c6 #3e3e3e;padding: 5px;box-sizing: border-box;background-color: rgba(239, 16, 16, 0.0980392);">
										<section
											style="display: inline-block;width: 100%;border-left: 0px solid rgb(202, 198, 198);border-right: 1px solid rgb(202, 198, 198);border-top-style: solid;border-bottom-style: solid;border-top-width: 0px;border-bottom-width: 0px;box-sizing: border-box;">
											<section style="font-size: 10px;box-sizing: border-box;">
												<p style="box-sizing: border-box;"> 日本</p>
											</section>
										</section>
									</section>
									<section
										style="display: table-cell;vertical-align: middle;width: 25%;border-width: 1px;border-radius: 0px;border-style: none none dashed;border-color: #3e3e3e #f8f8f8 #cac6c6 #3e3e3e;padding: 5px;box-sizing: border-box;background-color: rgba(239, 16, 16, 0.0980392);">
										<section style="font-size: 10px;box-sizing: border-box;">
											<p style="box-sizing: border-box;"> 75</p>
										</section>
									</section>
								</section>
							</section>
							<section style="display: table-row-group;box-sizing: border-box;">
								<section style="display: table-row;width: 100%;box-sizing: border-box;">
									<section
										style="display: table-cell;vertical-align: middle;width: 25%;border-width: 1px;border-radius: 0px;border-style: none none dashed;border-color: #3e3e3e #f8f8f8 #cac6c6 #3e3e3e;padding: 5px;box-sizing: border-box;background-color: rgba(239, 16, 16, 0.0235294);">
										<section
											style="display: inline-block;width: 100%;border-left: 0px solid rgb(202, 198, 198);border-right: 1px solid rgb(202, 198, 198);border-top-style: solid;border-bottom-style: solid;border-top-width: 0px;border-bottom-width: 0px;box-sizing: border-box;">
											<section style="font-size: 10px;box-sizing: border-box;">
												<p style="box-sizing: border-box;"> 季军</p>
											</section>
										</section>
									</section>
									<section
										style="display: table-cell;vertical-align: middle;width: 50%;border-width: 1px;border-radius: 0px;border-style: none none dashed;border-color: #3e3e3e #f8f8f8 #cac6c6 #3e3e3e;padding: 5px;box-sizing: border-box;background-color: rgba(239, 16, 16, 0.0235294);">
										<section style="box-sizing: border-box;">
											<section
												style="display: inline-block;width: 100%;border-left: 0px solid rgb(202, 198, 198);border-right: 1px solid rgb(202, 198, 198);border-top-style: solid;border-bottom-style: solid;border-top-width: 0px;border-bottom-width: 0px;box-sizing: border-box;">
												<section style="box-sizing: border-box;">
													<section style="font-size: 10px;box-sizing: border-box;">
														<p style="box-sizing: border-box;"> 韩国</p>
													</section>
												</section>
											</section>
										</section>
									</section>
									<section
										style="display: table-cell;vertical-align: middle;width: 25%;border-width: 1px;border-radius: 0px;border-style: none none dashed;border-color: #3e3e3e #f8f8f8 #cac6c6 #3e3e3e;padding: 5px;box-sizing: border-box;background-color: rgba(239, 16, 16, 0.0235294);">
										<section style="box-sizing: border-box;">
											<section style="font-size: 10px;box-sizing: border-box;">
												<p style="box-sizing: border-box;"> 49</p>
											</section>
										</section>
									</section>
								</section>
							</section>
						</section>
					</section>
				</section>
			</section>
		`,
  `<i class="fa fa-heart-o cursor-fav"></i><i class="cursor-vip v1"></i>
			<section
				style="padding-top: 10px;padding-right: 10px;padding-left: 35px;margin-left: 50px;border-left: 2px dotted rgb(12, 137, 24);box-sizing: border-box;">
				<section
					style="width: 50px;margin-left: -84px;border-right-width: 5px;border-right-style: solid;border-right-color: rgb(12, 137, 24);text-align: center;box-sizing: border-box;">
					<p style="line-height: 1.5em;font-size: 14px;color: rgb(12, 137, 24);"><strong>88</strong></p>
					<p style="line-height: 1.5em;font-size: 14px;color: rgb(12, 137, 24);">5月</p>
				</section>
				<section style="margin-top: -43px;padding-bottom: 10px;margin-left: -10px;box-sizing: border-box;">
					<p style="line-height: 1.5em;font-size: 16px;color: rgb(12, 137, 24);"><strong>发布2.0</strong></p>
					<p style="line-height: 1.5em;font-size: 14px;color: rgb(12, 137, 24);">新版本上线</p>
				</section>
			</section>
			<section style="box-sizing: border-box;">
				<section
					style="padding-top: 10px;padding-right: 10px;padding-left: 35px;margin-left: 50px;border-left: 2px dotted rgb(216, 40, 33);box-sizing: border-box;">
					<section
						style="width: 50px;margin-left: -84px;border-right: 5px solid rgb(216, 40, 33);text-align: center;box-sizing: border-box;">
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(216, 40, 33);"><strong>88</strong></p>
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(216, 40, 33);">10月</p>
					</section>
					<section style="margin-top: -43px;padding-bottom: 10px;margin-left: -10px;box-sizing: border-box;">
						<p style="line-height: 1.5em;font-size: 16px;color: rgb(216, 40, 33);"><strong>3.0发布</strong></p>
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(216, 40, 33);">微信编辑器图文上线</p>
					</section>
				</section>
			</section>
			<section style="box-sizing: border-box;">
				<section
					style="padding-top: 10px;padding-right: 10px;padding-left: 35px;margin-left: 50px;border-left: 2px dotted rgb(30, 155, 232);box-sizing: border-box;">
					<section
						style="width: 50px;margin-left: -84px;border-right: 5px solid rgb(30, 155, 232);text-align: center;box-sizing: border-box;">
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(30, 155, 232);"><strong>88</strong></p>
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(30, 155, 232);">4月</p>
					</section>
					<section style="margin-top: -43px;padding-bottom: 10px;margin-left: -10px;box-sizing: border-box;">
						<p style="line-height: 1.5em;font-size: 16px;color: rgb(30, 155, 232);"><strong>4.0版本发布</strong></p>
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(30, 155, 232);">使用签名功能上线</p>
					</section>
				</section>
			</section>
			<section style="box-sizing: border-box;">
				<section
					style="padding-top: 10px;padding-right: 10px;padding-left: 35px;margin-left: 50px;border-left: 2px dotted rgb(141, 75, 187);box-sizing: border-box;">
					<section
						style="width: 50px;margin-left: -84px;border-right: 5px solid rgb(141, 75, 187);text-align: center;box-sizing: border-box;">
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(141, 75, 187);"><strong>98</strong></p>
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(141, 75, 187);">9月</p>
					</section>
					<section style="margin-top: -43px;padding-bottom: 10px;margin-left: -10px;box-sizing: border-box;">
						<p style="line-height: 1.5em;font-size: 16px;color: rgb(141, 75, 187);"><strong>4.3版本发布</strong></p>
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(141, 75, 187);">微信编辑器图文上线</p>
					</section>
				</section>
			</section>
			<section style="box-sizing: border-box;">
				<section
					style="padding-top: 10px;padding-right: 10px;padding-left: 35px;margin-left: 50px;border-left: 2px dotted rgb(255, 129, 36);box-sizing: border-box;">
					<section
						style="width: 50px;margin-left: -84px;border-right: 5px solid rgb(255, 129, 36);text-align: center;box-sizing: border-box;">
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(255, 129, 36);"><strong>98</strong></p>
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(255, 129, 36);">1月</p>
					</section>
					<section style="margin-top: -43px;padding-bottom: 10px;margin-left: -10px;box-sizing: border-box;">
						<p style="line-height: 1.5em;font-size: 16px;color: rgb(255, 129, 36);"><strong>98用户突破</strong></p>
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(255, 129, 36);">用户破百万</p>
					</section>
				</section>
			</section>
			<section style="box-sizing: border-box;">
				<section
					style="padding-top: 10px;padding-right: 10px;padding-left: 35px;margin-left: 50px;border-left: 2px dotted rgb(128, 177, 53);box-sizing: border-box;">
					<section
						style="width: 50px;margin-left: -84px;border-right: 5px solid rgb(128, 177, 53);text-align: center;box-sizing: border-box;">
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(128, 177, 53);"><strong>88</strong></p>
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(128, 177, 53);">4月</p>
					</section>
					<section style="margin-top: -43px;padding-bottom: 10px;margin-left: -10px;box-sizing: border-box;">
						<p style="line-height: 1.5em;font-size: 16px;color: rgb(128, 177, 53);"><strong>赞赏功能</strong></p>
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(128, 177, 53);">iOS版平台及表情赞赏功能被关闭</p>
						<p style="line-height: 1.5em;font-size: 14px;color: rgb(128, 177, 53);"></p>
					</section>
				</section>
			</section>
		`,
  `
			<i class="fa fa-heart-o cursor-fav"></i>
			<i class="cursor-vip v1"></i>
			<section
				style="border-left: 2px solid rgb(200, 200, 200);border-top: 1px solid transparent;margin-left: 5%;box-sizing: border-box;">
				<section
					style="width: 16px;height: 16px;border-width: 2px;border-style: solid;border-color: rgb(160, 160, 160);border-radius: 50%;margin-left: -9px;margin-top: 20px;background-color: rgb(235, 103, 148);box-sizing: border-box;">
				</section>
				<section style="margin-left:20px;margin-top:-20px;">
					<section style="color:rgb(80,80,80);">
						<p style="font-size: 16px;color: rgb(235, 103, 148);"><strong>《致橡树》</strong></p>
					</section>
					<section
						style="padding-top: 5px;padding-bottom: 15px;text-align: justify;letter-spacing: 2px;color: rgb(80, 80, 80);box-sizing: border-box;">
						<p style="font-size: 14px;">我必须是你近旁的一株木棉，作为树的形象和你站在一起。</p>
					</section>
				</section>
				<section
					style="width: 16px;height: 16px;border-width: 2px;border-style: solid;border-color: rgb(160, 160, 160);border-radius: 50%;margin-left: -9px;margin-top: 20px;background-color: rgb(235, 103, 148);box-sizing: border-box;">
				</section>
				<section style="margin-left:20px;margin-top:-20px;">
					<section style="color:rgb(80,80,80);">
						<p style="font-size: 16px;color: rgb(235, 103, 148);"><strong>《回答》</strong></p>
					</section>
					<section
						style="padding-top: 5px;padding-bottom: 15px;text-align: justify;letter-spacing: 2px;color: rgb(80, 80, 80);box-sizing: border-box;">
						<p style="font-size: 14px;">我相信我们都通晓一种语言。花钟喑哑的铃声，陨星没有写完的诗，以及录音带所无法窃听的——霞光殷红的远方给予你我的暗示。如果一定要说话，我无言以答。
						</p>
					</section>
				</section>
				<section
					style="width: 16px;height: 16px;border-width: 2px;border-style: solid;border-color: rgb(160, 160, 160);border-radius: 50%;margin-left: -9px;margin-top: 20px;background-color: rgb(235, 103, 148);box-sizing: border-box;">
				</section>
				<section style="margin-left:20px;margin-top:-20px;">
					<section style="color:rgb(80,80,80);">
						<p style="font-size: 16px;color: rgb(235, 103, 148);"><strong>《会唱歌的鸢尾花》</strong></p>
					</section>
					<section
						style="padding-top: 5px;padding-bottom: 15px;text-align: justify;letter-spacing: 2px;color: rgb(80, 80, 80);box-sizing: border-box;">
						<p style="font-size: 14px;">我的忧伤，因为有你的照耀，升起一圈淡淡的光轮。</p>
					</section>
				</section>
			</section>
		`,

  `<i class="fa fa-heart-o cursor-fav"></i><i class="cursor-vip v1"></i>
			<section style="display: inline-block;vertical-align: middle;width: 9%;box-sizing: border-box;">
				<section>
					<section style="margin-top: 0.5em;margin-bottom: 0.5em;box-sizing: border-box;">
						<section style="border-top: 1px dotted #5a6272;box-sizing: border-box;"></section>
					</section>
				</section>
			</section>
			<section style="display: inline-block;vertical-align: middle;width: 20%;box-sizing: border-box;">
				<section>
					<section style="text-align: center;margin-top: 10px;margin-bottom: 10px;box-sizing: border-box;">
						<section
							style="max-width: 100%;vertical-align: middle;display: inline-block;width: 75%;box-sizing: border-box;">
							<img referrerpolicy="no-referrer"
								src="http://qiniu.other.88an.top/wxedit/20180918_wxeditor_e47936532.gif"
								style="vertical-align: middle;box-sizing: border-box;width: 100%;">
						</section>
					</section>
				</section>
			</section>
			<section style="display: inline-block;vertical-align: middle;width: 71%;box-sizing: border-box;">
				<section>
					<section style="margin-top: 0.5em;margin-bottom: 0.5em;box-sizing: border-box;">
						<section style="border-top: 1px dotted #5a6272;box-sizing: border-box;"></section>
					</section>
				</section>
			</section>
			<section>
				<section style="box-sizing: border-box;">
					<section
						style="text-align: center;font-size: 14px;padding-right: 10px;padding-left: 10px;letter-spacing: 1.5px;box-sizing: border-box;">
						<p style="box-sizing: border-box;text-align: left;">
							昙花一现，蜉蝣朝生暮死，都有过最美的一瞬，人的一生相对于万物的永恒来说，却不过弹指一瞬，或许抽点时间来学习工作，抽点儿时间去思考，抽点儿时间去娱乐，抽点儿时间去读书。</p>
					</section>
				</section>
			</section>
			<section>
				<section style="box-sizing: border-box;">
					<section style="display: inline-block;vertical-align: middle;width: 71%;box-sizing: border-box;">
						<section>
							<section style="margin-top: 0.5em;margin-bottom: 0.5em;box-sizing: border-box;">
								<section style="border-top: 1px dotted #5a6272;box-sizing: border-box;"></section>
							</section>
						</section>
					</section>
					<section style="display: inline-block;vertical-align: middle;width: 20%;box-sizing: border-box;">
						<section>
							<section style="text-align: center;margin: 10px 0%;box-sizing: border-box;">
								<section
									style="max-width: 100%;vertical-align: middle;display: inline-block;width: 75%;box-sizing: border-box;">
									<img referrerpolicy="no-referrer"
										src="http://qiniu.other.88an.top/wxedit/20180918_wxeditor_e47936532.gif"
										style="vertical-align: middle;box-sizing: border-box;width: 100%;">
								</section>
							</section>
						</section>
					</section>
					<section style="display: inline-block;vertical-align: middle;width: 9%;box-sizing: border-box;">
						<section>
							<section style="margin-top: 0.5em;margin-bottom: 0.5em;box-sizing: border-box;">
								<section style="border-top: 1px dotted #5a6272;box-sizing: border-box;"></section>
							</section>
						</section>
					</section>
				</section>
			</section>
		`,

  `
			<blockquote
				style="padding: 15px 25px; margin: 0px; color: rgb(51, 51, 51); font-family: 微软雅黑; white-space: normal; max-width: 100%; word-wrap: break-word; top: 0px; line-height: 24px;  vertical-align: baseline; border-left-color: rgb(0, 187, 236); border-left-width: 10px; border-left-style: solid; background-color: rgb(239, 239, 239);">
				<p style="padding: 0px; margin-top: 0px; margin-bottom: 0px;">可在这输入内容，wwei微信编辑器，微信编辑首选。</p>
			</blockquote>
		`,
  `
			<blockquote
				style="padding: 5px 15px; margin: 0px; font-family: 微软雅黑;  white-space: normal; max-width: 100%; color: rgb(255, 255, 255); line-height: 25px; font-weight: bold; border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 0px; border-bottom-left-radius: 0px; border: 0px; background-color: rgb(0, 187, 236);">
				<span style="font-size: 16px;">这输入标题</span>
			</blockquote>
			<blockquote
				style="padding: 10px 15px 20px; margin: 0px; color: rgb(51, 51, 51); font-family: 微软雅黑;  white-space: normal; max-width: 100%; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; border: 0px; line-height: 25px; background-color: rgb(239, 239, 239);">
				<p style="padding: 0px; margin-top: 0px; margin-bottom: 0px;">可在这输入内容，wwei微信编辑器，微信编辑首选。</p>
			</blockquote>
		`,

  `
			<section class="wwei-editor">
				<fieldset style="border: 0px; margin: 5px 0px; box-sizing: border-box; padding: 0px;">
					<section style="height: 1em; box-sizing: border-box;">
						<section
							style="height: 100%; width: 1.5em; float: left; border-top-width: 0.4em; border-top-style: solid; border-color: rgb(249, 110, 87); border-left-width: 0.4em; border-left-style: solid; box-sizing: border-box;">
						</section>
						<section
							style="height: 100%; width: 1.5em; float: right; border-top-width: 0.4em; border-top-style: solid; border-color: rgb(249, 110, 87); border-right-width: 0.4em; border-right-style: solid; box-sizing: border-box;">
						</section>
						<section style="display: inline-block; color: transparent; clear: both; box-sizing: border-box;">
						</section>
					</section>
					<section
						style="margin: -0.8em 0.1em -0.8em 0.2em; padding: 0.8em; border: 1px solid rgb(249, 110, 87); border-radius: 0.3em; box-sizing: border-box;">
						<section placeholder="四角宽边框的样式"
							style="color: rgb(51, 51, 51); font-size: 1em; line-height: 1.4; word-break: break-all; word-wrap: break-word;"
							class="wweibrush">四角宽边框的样式</section>
					</section>
					<section style="height: 1em; box-sizing: border-box;">
						<section
							style="height: 100%; width: 1.5em; float: left; border-bottom-width: 0.4em; border-bottom-style: solid; border-color: rgb(249, 110, 87); border-left-width: 0.4em; border-left-style: solid; box-sizing: border-box;">
						</section>
						<section
							style="height: 100%; width: 1.5em; float: right; border-bottom-width: 0.4em; border-bottom-style: solid; border-color: rgb(249, 110, 87); border-right-width: 0.4em; border-right-style: solid; box-sizing: border-box;">
						</section>
					</section>
				</fieldset>
			</section>
		`,
  `
			<section class="wwei-editor">
				<blockquote class="wweibrush"
					style="white-space: normal;font-size: 14px; line-height: 25px; margin: 2px 0px; padding: 10px 10px; border: 2px dashed #dedcde;max-width: 100%;">
					<p placeholder="虚线框内容，作为摘要或段落内容。">虚线框内容，作为摘要或段落内容。</p>
				</blockquote>
			</section>
		`,
  `
			<section class="wwei-editor">
				<section class="wweibrush"
					style="margin: 3px; padding: 15px;color: rgb(62, 62, 62); line-height: 24px;box-shadow: rgb(170, 170, 170) 0px 0px 3px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); -webkit-box-shadow: rgb(170, 170, 170) 0px 0px 3px;">
					<p>边框阴影内容区域</p>
				</section>
			</section>
		`,
  `
			<section class="wwei-editor">
				<blockquote
					style="border-width: 1px 1px 1px 5px; border-style: solid; border-color: rgb(238, 238, 238) rgb(238, 238, 238) rgb(238, 238, 238) rgb(159, 136, 127); border-radius: 3px; padding: 10px; margin: 10px 0px;">
					<h4 class="wweibrush"
						style="color: rgb(159, 136, 127); font-weight: bold; font-size: 18px; margin: 5px 0px; border-color: rgb(159, 136, 127);">
						标题文字</h4>
					<section class="wweibrush" data-style="color:inherit;font-size:14px;"
						style="color: inherit;font-size:14px">
						<p>内容描述.</p>
					</section>
				</blockquote>
			</section>
		`,
  `
			<section class="wwei-editor">
				<section
					style="color: inherit; font-size: 16px; padding: 5px 10px 0px 35px; margin-left: 27px; border-left-width: 2px; border-left-style: dotted; border-left-color: rgb(228, 228, 228);">
					<section class="autonum"
						style="width: 32px; height: 32px; margin-left: -53px; margin-top: 23px; color: rgb(202, 251, 215); text-align: center; line-height: 32px; border-top-left-radius: 16px; border-top-right-radius: 16px; border-bottom-right-radius: 16px; border-bottom-left-radius: 16px; background: rgb(14, 184, 58);">
						1</section>
					<section class="wweibrush" style="margin-top: -30px;padding-bottom: 10px; color: inherit;">
						<p style="clear: both; line-height: 1.5em; font-size: 14px; color: inherit;"><span
								style="color:inherit; font-size:16px"><strong
									style="color:inherit">如何进入【微信编辑器】？</strong></span></p>
						<p style="clear: both; line-height: 1.5em; font-size: 14px; color: inherit;">
							网页搜索“微信编辑器”，点击第一条搜索结果即可进入编辑器页面</p>
					</section>
				</section>
			</section>
		`,
  `
			<section class="wwei-editor">
				<blockquote
					style="margin: 3px; padding: 10px 15px; border-width: 3px; border-color: rgb(107, 77, 64); border-top-left-radius: 50px; border-bottom-right-radius: 50px; box-shadow: rgb(165, 165, 165) 5px 5px 2px; background-color: rgb(250, 250, 250);">
					<section class="wweibrush"
						data-style="margin-top: 15px; margin-bottom: 0px; padding: 0px; color: rgb(107, 77, 64); line-height: 2em; font-size: 20px; border-color: rgb(107, 77, 64);font-size: 18px;font-weight:bold;">
						<p
							style="margin-top: 15px; margin-bottom: 0px; padding: 0px; color: rgb(107, 77, 64); line-height: 2em; font-size: 20px; border-color: rgb(107, 77, 64);">
							<span style="font-size:18px"><strong
									style="border-color:rgb(107, 77, 64); color:inherit">读而思</strong></span>
						</p>
					</section>
					<p
						style="margin-top: -10px; margin-bottom: 0px; padding: 0px; line-height: 2em; min-height: 1.5em; color: inherit;">
						<span style="font-size:12px"><strong class="wweibrush" data-brushtype="text"
								style="background-color:rgb(107, 77, 64); border-bottom-left-radius:20px; border-bottom-right-radius:20px; color:rgb(224, 209, 202); font-size:13px; padding:0px 15px">duersi</strong></span>
					</p>
					<section class="wweibrush"
						data-style="margin-top: 0px; margin-bottom: 0px; padding: 0px; line-height: 2em; font-size: 14px; min-height: 1.5em; color: inherit;">
						<p><span style="font-size:14px">编辑完成后，将内容复制粘贴到微信后台素材管理的编辑器中即可。</span></p>
						<p
							style="margin-top: 0px; margin-bottom: 0px; padding: 0px; line-height: 2em; font-size: 16px; min-height: 1.5em; color: inherit;">
							<span style="font-size:14px"></span>
						</p>
					</section>
				</blockquote>
			</section>
		`,
  `
			<section class="wwei-editor">
				<fieldset
					style="margin: 2em 1em 1em; padding: 0px; border: 0px rgb(255, 179, 167); border-image-source: none; max-width: 100%; box-sizing: border-box; color: rgb(62, 62, 62); font-size: 16px; line-height: 25px; word-wrap: break-word !important;">
					<section
						style="max-width: 100%; word-wrap: break-word !important; box-sizing: border-box; line-height: 1.4; margin-left: -0.5em;">
						<section
							style="max-width: 100%; box-sizing: border-box; border-color: rgb(0, 0, 0); padding: 3px 8px; border-radius: 4px;color: rgb(167, 23, 0); font-size: 1em;display: inline-block; -webkit-transform: rotateZ(-10deg);transform: rotate(-10deg);transform-origin: left center 0; -webkit-transform-origin: 0% 100%; word-wrap: break-word !important; background-color: rgb(255, 179, 167);">
							<span style="color:rgb(255, 255, 255)">微信编辑器</span>
						</section>
					</section>
					<section class="wweibrush"
						style="max-width: 100%; box-sizing: border-box; padding: 22px 16px 16px; border: 1px solid rgb(255, 179, 167);color: rgb(0, 0, 0); font-size: 14px;margin-top: -24px;">
						<p style="line-height:24px;"><span
								style="color:rgb(89, 89, 89); font-size:14px">微信编辑器提供非常好用的微信图文编辑器。可以随心所欲的变换颜色调整格式，更有神奇的自动配色方案。</span>
						</p>
					</section>
				</fieldset>
			</section>
		`,
  `
			<section class="wwei-editor">
				<fieldset
					style="margin: 0px; padding: 5px; border: 1px solid rgb(204, 204, 204); max-width: 100%; color: rgb(62, 62, 62); line-height: 24px; text-align: justify; box-shadow: rgb(165, 165, 165) 5px 5px 2px; background-color: rgb(250, 250, 250);">
					<legend style="margin: 0px; padding: 0px;  text-align: left;margin-left: 20px;width: auto;">
						<strong><strong
								style="background-color:rgb(255, 255, 245); color:rgb(102, 102, 102); line-height:20px"><span
									class="wweibrush" data-brushtype="text"
									style="background-color:red; border-bottom-left-radius:3em 1em; border-bottom-right-radius:0.5em 2em; border-top-left-radius:0.5em 4em; border-top-right-radius:3em 1em; box-shadow:rgb(165, 165, 165) 4px 4px 2px; color:white; max-width:100%; padding:4px 10px; text-align:justify">公告通知</span></strong></strong>&nbsp;&nbsp;
					</legend>
					<section class="wweibrush"
						data-style="margin-top: 2px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; line-height: 2em;font-weight:bold;">
						<p
							style="margin-top: 0px; margin-bottom: 0px; padding: 0px; max-width: 100%; min-height: 1.5em; line-height: 2em;">
							各位小伙伴们，微信图文美化编辑器正式上线了，欢迎大家多使用多提供反馈意见。使用<span
								style="color:inherit"><strong>谷歌与火狐浏览器</strong></span>，可获得与手机端一致的显示效果。ie内核的低版本浏览器可能有不兼容的情况
						</p>
					</section>
				</fieldset>
			</section>
		`,
  `
			<section class="wwei-editor">
				<section style="max-width: 100%; margin: 2px; padding: 0px;">
					<section style="max-width: 100%;margin-left:1em; line-height: 1.4em;"><span
							style="font-size:14px"><strong
								style="color:rgb(62, 62, 62); line-height:32px; white-space:pre-wrap"><span
									class="wweibrush" data-brushtype="text" data-mce-style="color: #a5a5a5;"
									placeholder="关于微信编辑器"
									style="background-color:rgb(86, 159, 8); border-radius:5px; color:rgb(255, 255, 255); padding:4px 10px">关于微信编辑器</span></strong></span>&nbsp;&nbsp;
					</section>
					<section class="wweibrush" data-style="color:rgb(89, 89, 89); font-size:14px; line-height:28px"
						style="font-size: 1em; max-width: 100%; margin-top: -0.7em; padding: 10px 1em; border: 1px solid rgb(192, 200, 209); border-radius: 0.4em; color: rgb(51, 51, 51); background-color: rgb(239, 239, 239);">
						<p><span placeholder="提供非常好用的微信文章编辑工具。">非常好用的在线图文编辑工具</span>&nbsp;&nbsp;&nbsp;&nbsp;</p>
					</section>
				</section>
			</section>
		`,
  `
			<section class="wwei-editor">
				<fieldset
					style="margin: 0.5em 0px; padding: 0px; max-width: 100%; box-sizing: border-box; color: rgb(62, 62, 62); line-height: 25px; white-space: normal; border: 0px rgb(238, 222, 176); word-wrap: break-word !important;">
					<section
						style="margin: 0px; padding: 0px; width: 100%; box-sizing: border-box;display: inline-block; text-align: center; word-wrap: break-word !important;">
						<img src="http://qiniu.other.88an.top/wxedit/06.png"
							style="box-sizing:border-box; color:inherit; height:65px; margin:0px auto; padding:0px; visibility:visible !important; width:60px; word-wrap:break-word !important" />
					</section>
					<section
						style="margin: -2.3em 0px 0px; padding: 2em 0px 0px; max-width: 100%; box-sizing: border-box; min-height: 15em; font-size: 1em;  font-weight: inherit; text-decoration: inherit; color: rgb(131, 104, 28); border-color: rgb(238, 222, 176); word-wrap: break-word !important;  background-color: rgb(238, 222, 176); background-repeat: repeat;">
						<section
							style="margin: 0.3em auto; padding: 0.5em; max-width: 100%; box-sizing: border-box; width: 7em; height: 3.5em; line-height: 2em; overflow: hidden; -webkit-transform: rotate(-5deg); font-size: 32px;  font-weight: inherit; text-align: center; text-decoration: inherit; color: inherit; word-wrap: break-word !important; background-repeat: no-repeat;background-size: contain;">
							<section
								style="margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box; word-wrap: break-word !important;">
								<span
									style="box-sizing:border-box; color:inherit; margin:0px; max-width:100%; padding:0px; word-wrap:break-word !important">公告</span>
							</section>
						</section>
						<section
							style="margin: 0px; padding: 1em; max-width: 100%; box-sizing: border-box; word-wrap: break-word !important;">
							<section class="wweibrush"
								style="margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box; word-wrap: break-word !important; color: inherit;">
								<p>本背景可以换色哦~</p>
							</section>
						</section>
					</section>
				</fieldset>
				<p><br /></p>
			</section>
		`,
  `
			<section class="wwei-editor">
				<blockquote
					style="white-space: normal; margin: 5px 0px 0px; padding: 10px; max-width: 100%; border-left-width: 5px; border-left-style: solid; border-left-color: rgb(0, 176, 80); line-height: 25px; color: rgb(102, 102, 102);">
					<p class="wweibrush" placeholder="请输入内容内容。">请输入内容内容。</p>
				</blockquote>
			</section>
		`,
  `
			<section class="wwei-editor">
				<section
					style="margin: 10px 0px; padding: 50px 0px; color: rgb(202, 251, 215); text-align: center; background-color: rgb(14, 184, 58);">
					<span
						style="border:1px solid rgb(202, 251, 215); font-size:18px; line-height:42px; padding:10px 15px">微信编辑器</span>
					<section class="wweibrush" style="margin-top:30px;">
						<p>秒刷，最易用的图文排版工具</p>
					</section>
				</section>
			</section>
		`,
  `
			<section class="wwei-editor">
				<section
					style="padding: 0px 8px; border-left-width: 3px; border-left-style: solid; border-color: rgba(163, 163, 163, 0.843137); font-size: 22px; font-weight: inherit; text-align: inherit; text-decoration: inherit; box-sizing: border-box;">
					<section style="line-height: 1.4; box-sizing: border-box; color: inherit;">
						<section class="wweibrush" data-brushtype="text"
							style="border-color: rgb(117, 117, 118); color: rgb(117, 117, 118); font-size: 20px;">标题
						</section>
					</section>
					<section class="wweibrush"
						data-style="border-color: rgb(117, 117, 118); color: inherit; font-size: 12px;"
						style="color: rgb(117, 117, 118); line-height: 1.4; margin-top: 5px; padding-left: 3px; font-size: 14px; font-weight: inherit; text-align: inherit; text-decoration: inherit; box-sizing: border-box; border-color: rgb(117, 117, 118);">
						<p style="border-color: rgb(117, 117, 118); color: inherit; font-size: 12px;">内容描述</p>
					</section>
				</section>
			</section>
		`,
  `
			<section class="wwei-editor">
				<p class="wweibrush" data-brushtype="text"
					style="margin: 2px 5px 0px 0px; padding: 0px;color:rgb(75, 92, 196); float:left; font-size:2.7em; line-height:1em; margin-right:5px">
					秒刷</p>
				<section class="wweibrush" data-style="clear:none;">
					<p style="clear:none;">
						选择需要应用样式的文字，然后选择要使用的样式，即可实现秒刷效果。秒刷支持所有样式，如有使用遇到问题，欢迎加入QQ群<strong>123456789</strong>，将问题反馈给我们</p>
					<p style="clear:none;color:red;">回车使下沉占两行的文字独自为一个段落，然后再使用秒刷。建议下沉的为一个或者2个文字，不要多了。</p>
				</section>
			</section>
		`,
  `
			<section class="wwei-editor">
				<fieldset
					style="padding: 5px; border: 1px solid rgb(204, 204, 204); line-height: 24px; color: inherit; background-color: rgb(254, 254, 254);">
					<legend style="margin: 0px 0px 0px 15px; padding: 0px; width: auto; font-size: 16px; color: inherit;">
						<span style="color:inherit; margin:0px; padding:0px"><strong
								style="color:rgb(102, 102, 102); margin:15px 8px 0px 0px"><span class="wweibrush"
									data-brushtype="text"
									style="background-color:rgb(145, 168, 252); border-color:rgb(145, 168, 252); border-radius:5px; color:rgb(255, 255, 255); padding:4px 10px">An-admin</span>&nbsp;</strong><span
								class="wweibrush" data-brushtype="text"
								style="border-color:rgb(145, 168, 252); color:rgb(145, 168, 252); margin:0px; padding:0px">AN-admin&nbsp;</span></span>
					</legend>
					<section class="wweibrush" data-style="text-indent: 2em;;"
						style="margin: 15px; margin-bottom: 0px; padding: 0px; line-height: 2em; color: rgb(62, 62, 62); font-size: 14px;">
						<p style="text-indent: 2em; color: inherit;">
							选择需要应用样式的文字，然后选择要使用的样式，即可实现秒刷效果。秒刷支持所有样式</p>
					</section>
				</fieldset>
			</section>
		`,
  `
			<section class="wwei-editor">
				<section>
					<section style="width:48%;display:inline-block;float:left;">
						<section style="padding:20px 25px;border:1px solid #e7e7e7;text-align:center;">
							<p><img src="http://qiniu.other.88an.top/wxedit/logo-135-web.png"
									style="height:50px; margin-bottom:15px" /></p>
							<h3 class="134title" style="font-size:16px;font-weight:bold;margin:0 0 10px 0;">微信编辑器</h3>
							<section class="wweibrush" data-style="clear:none;">
								<p style="clear:none;margin:0 0;line-height:1.5em;">生而排版</p>
								<p style="clear:none;margin:0 0;line-height:1.5em;"><span
										style="text-align:center">为你而美</span></p>
							</section>
						</section>
					</section>
					<section style="margin-left:4%;width:48%;display:inline-block;text-align:center;">
						<section style="padding:20px 25px;border:1px solid #e7e7e7;">
							<p><img src="http://qiniu.other.88an.top/wxedit/0(6).jpg"
									style="height:50px; margin-bottom:15px" /></p>
							<h3 class="134title" style="font-size:16px;font-weight:bold;margin:0 0 10px 0;">秒刷</h3>
							<section class="wweibrush" data-style="clear:none;margin:0 0;line-height:1.5em;">
								<p style="clear:none;margin:0 0;line-height:1.5em;">一键排版</p>
								<p style="clear:none;margin:0 0;line-height:1.5em;">珍惜生命</p>
							</section>
						</section>
					</section>
					<p style="height:2px;clear:both;"><br /></p>
				</section>
			</section>
		`,

  `
			<section class="wwei-editor">
				<section
					style="max-width: 100%; float: left; margin: 0px 15px 10px; color: inherit; box-sizing: border-box !important; word-wrap: break-word !important;">
					<p
						style="margin-top: 0px; margin-bottom: 0px; line-height: 72px; max-width: 100%; clear: both; color: inherit; box-sizing: border-box !important; word-wrap: break-word !important;">
						<span
							style="box-sizing: border-box !important; max-width: 100%; word-wrap: break-word !important; color: rgb(95, 156, 239); font-size: 72px;">08</span>
					</p>
					<p
						style="margin-top: 0px; margin-bottom: 0px; line-height: 1.6; max-width: 100%; clear: both; color: rgb(153, 153, 153); box-sizing: border-box !important; word-wrap: break-word !important;">
						<span
							style="box-sizing: border-box !important; max-width: 100%; word-wrap: break-word !important; color: inherit; font-size: 24px;">2015/05&nbsp;</span>
					</p>
				</section>
				<section
					style="box-sizing: border-box !important; display: inline-block; max-width: 100%; word-wrap: break-word !important; margin: 24px 5px -20px -8px; padding: 0px; border-right-width: 5px; border-left-width: 0px; border-right-style: solid; border-right-color: rgb(95, 156, 239); border-left-color: rgb(95, 156, 239); height: 5px; width: 5px; vertical-align: top; float: left; border-bottom-width: 5px !important; border-top-style: solid !important; border-bottom-style: solid !important; border-top-color: transparent !important; border-bottom-color: transparent !important;">
				</section>
				<section data-brushtype="text"
					style="max-width: 100%; margin-left: 125px; line-height: 1.75em; color: rgb(255, 255, 255); padding: 15px; border-radius: 5px; box-sizing: border-box !important; word-wrap: break-word !important; background: rgb(95, 156, 239);">
					<span
						style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;">日历场景风格，代办事项，描述您人生精彩的一天记录自己的人生，wwei编辑器为您服务。</span>
				</section>
			</section>
		`,

  `
			<section class="wwei-editor">
				<section data-brushtype="text"
					style="max-width: 100%; color: rgb(70, 70, 72); padding: 0px;  margin: 0px auto; box-sizing: border-box !important; word-wrap: break-word !important;">
					<section data-brushtype="text"
						style="box-sizing: border-box !important; display: inline-block; max-width: 100%; word-wrap: break-word !important; width: 338px; margin: 0px; border-color: rgb(198, 198, 199); color: inherit; padding: 0px;">
						<p
							style="margin-top: 0px; margin-bottom: 0px; line-height: 1.6; max-width: 100%; clear: both; padding: 0px; box-sizing: border-box !important; word-wrap: break-word !important; background-color: #ccc">
							<span
								style=" box-sizing: border-box !important; max-width: 100%; word-wrap: break-word !important; color: #f30;">&nbsp;2015春装新款女学生打底衫短款</span>
						</p><img src="http://qiniu.other.88an.top/wxedit/0yifu.jpg"
							style="box-sizing: border-box !important; border: 0px; vertical-align: middle; margin: 0px; padding: 0px; max-width: 100%; word-wrap: break-word !important; height: auto !important; width: 338px;" />
					</section>
				</section>
			</section>
		`,

  `
			<section class="wwei-editor">
				<blockquote
					style="white-space: normal; margin: 0px; padding: 12px 15px; border: 0px solid rgb(208, 218, 254); color: rgb(0, 45, 207); min-height: 1em; text-align: justify; background-image: -webkit-linear-gradient(left, rgb(207, 217, 255), rgb(158, 179, 253), rgb(207, 217, 255)); background-color: rgb(255, 255, 240);">
					<section class="wweibrush" style="border-color: rgb(208, 218, 254); color: inherit;">
						<p style="border-color: rgb(208, 218, 254); color: inherit;">渐变背景内容</p>
					</section>
				</blockquote>
			</section>
		`,
  `
			<section class="wwei-editor">
				<section style="background-color: #fff;border: 1px solid #dbdbdb;margin: 0 0 2px 4px;"><img alt=""
						src="http://qiniu.other.88an.top/wxedit/0(5).jpg" style="width:100%" title="" vspace="0"
						border="0" />
					<h2 class="wweibrush" data-brushtype="text" style="margin:10px 0;font-size:20px;padding: 3px 10px 2px;">
						图片标题</h2>
					<section class="wweibrush" style="padding: 3px 10px 15px;">
						<p>图片内容描述，支持多段落。</p>
					</section>
				</section>
			</section>
		`,

  `
			<section class="wwei-editor">
				<section style="margin: 3px;background-color: #fff;box-shadow: 0 1px 3px rgba(0, 0, 0, 0.3);"><img
						src="http://qiniu.other.88an.top/wxedit/0(3).jpg" style="width:100%" />
					<section class="wweibrush" data-type="main">
						<p
							style="line-height: 1.35em; margin-top: 10px; overflow: hidden; padding: 0px 16px; word-wrap: break-word;">
							<span style="line-height:1.35em">图文样式排版。</span></p>
						<p
							style="line-height: 1.35em; margin-top: 10px; overflow: hidden; padding: 0px 16px; word-wrap: break-word;">
							底下为作者信息</p>
					</section>
					<section
						style="background: none repeat scroll 0 0 #fafafa;border-top: 1px solid #f2f2f2;color: #999;padding: 16px; 15px;">
						<img src="http://qiniu.other.88an.top/wxedit/0(4).jpg"
							style="float:left; height:34px; margin-right:10px; width:34px" />
						<section class="wweibrush" data-style="clear: none;line-height:17px;padding:0 0;font-size:12px;">
							<p style="clear: none;font-size:12px;line-height:17px;padding:0 0;margin:0 0;">
								<strong>简单易用的才是最好的</strong></p>
							<p style="clear: none;font-size:12px;line-height:17px;padding:0 0;margin:0 0;">微信编辑器</p>
						</section>
					</section>
				</section>
			</section>
		`,
  `
			<section class="wwei-editor">
				<section
					style="font-size: 16px; white-space: normal; margin:10px 0px; border: 1px solid rgb(201, 201, 201); text-align: center;">
					<section
						style="border: 0.1em solid rgb(255, 202, 0); margin: 1em auto; width: 12em; height: 12em; border-radius: 6em;">
						<section
							style="display: table; max-height: 11em; background-color: rgb(255, 202, 0); border-radius: 5.5em; height: 11em; width: 11em; margin: 0.5em;">
							<section class="wweibrush" data-brushtype="text"
								style="display: table-cell; vertical-align: middle; max-height: 11em; margin: 1em; padding: 1em; color: rgb(255, 255, 255); font-size: 1.5em; line-height: 1.2em; ">
								微信<br />编辑器</section>
						</section>
					</section>
					<section class="wweibrush" data-style="font-size: 1em;"
						style="display: inline-block; color: white; background-color: rgb(255, 202, 0); line-height: 1em; border-radius: 1em; margin: 1em 1em 2em; max-width: 100%; padding: 0.5em 1em; font-size: 1em;">
						<p>微信编辑器，您的微信文章可以更精彩！</p>
					</section>
				</section>
			</section>
		`,
  `
			<section class="wwei-editor">
				<fieldset style="border: none; margin: 0.8em 0px 0.3em; box-sizing: border-box; padding: 0px;">
					<section class="wweibrush" data-brushtype="text"
						style="width: 2.5em; height: 2.5em; margin: 0px auto 1px; border-radius: 100%; line-height: 2.5em; box-sizing: border-box; overflow: hidden; font-size: 40px;  text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); background-color: rgb(195, 39, 43); border-color: rgb(195, 39, 43);">
						微信</section>
					<section
						style="font-size: 1em;  font-weight: bold; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); border-color: rgb(195, 39, 43); box-sizing: border-box;">
						<section
							style="width: 0px; margin: 0px auto; border-bottom: 0.5em solid rgb(195, 39, 43); border-top-color: rgb(195, 39, 43); border-left: 1em solid transparent ! important; border-right: 1em solid transparent ! important; box-sizing: border-box;height: 10px;">
						</section>
						<section class="wweibrush" data-brushtype="text"
							style="padding: 0.5em; line-height: 1.2em; font-size: 1em;  box-sizing: border-box; background-color: rgb(195, 39, 43);">
							做最好用的编辑器</section>
					</section>
				</fieldset>
			</section>
		`,
];
